package Tasks.Medium.DFS_BFS;

/* 694. Number of Distinct Islands */

import java.util.*;

public class LC_694 {
    public static int numDistinctIslands(int[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) { return 0; }

        int[][] directions = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};
        Set<ArrayList<Integer>> islandsPrints = new HashSet<>();

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {

                    Queue<int[]> queue = new LinkedList<>();
                    queue.add(new int[] {i, j});
                    ArrayList<Integer> islandFingerPrint = new ArrayList<>();

                    while (!queue.isEmpty()) {
                       int[] currentPoint = queue.poll();
                        grid[currentPoint[0]][currentPoint[1]] = 0;
                        int fingerPrint = 0;
                        islandFingerPrint.add(fingerPrint);
                        for (int[] direction: directions) {
                            int x = currentPoint[0] + direction[0];
                            int y = currentPoint[1] + direction[1];
                            if (isNextMovePossible(x, y, grid, ++fingerPrint, islandFingerPrint)) {
                                queue.add(new int[] {x, y});
                            }
                        }
                    }

                    islandsPrints.add(islandFingerPrint);
                }
            }
        }

        return islandsPrints.size();
    }

    private static boolean isNextMovePossible(int i,
                                              int j,
                                              int[][] grid,
                                              int fingerPrint,
                                              ArrayList<Integer> islandFingerPrint) {

        if (i < 0 || i >= grid.length || j < 0 || j >= grid[i].length || grid[i][j] == 0) { return false; }

        grid[i][j] = 0;
        islandFingerPrint.add(fingerPrint);
        return true;
    }
}
