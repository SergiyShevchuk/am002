package Tasks.Medium.DFS_BFS;

/* 130. Surrounded Regions */

public class LC_130 {
    public static void solve(char[][] board) {
        if (board == null || board.length == 0 || board[0].length == 0) { return; }

        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        for (int i = 0; i < board[0].length; i++) {
            if (board[0][i] == 'O') {
                BFSSearch(board, 0, i, directions);
            }
            if (board[board.length - 1][i] == 'O') {
                BFSSearch(board, board.length - 1, i, directions);
            }
        }

        for (int i = 0; i < board.length; i++) {
            if (board[i][0] == 'O') {
                BFSSearch(board, i, 0, directions);
            }
            if (board[i][board[0].length - 1] == 'O') {
                BFSSearch(board, i, board[0].length - 1, directions);
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == 'V') {
                    board[i][j] = 'O';
                }

                if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
            }
        }
    }

    private static void BFSSearch(char[][] board, int i, int j, int[][] directions) {

        if (i < 0 ||
                i >= board.length ||
                j < 0 || j >= board[i].length ||
                board[i][j] == 'X' ||
                board[i][j] == 'V') { return; }

        board[i][j] = 'V';

        for (int[] direction: directions) {
          BFSSearch(board, i + direction[0], j + direction[1], directions);
        }
    }
}
