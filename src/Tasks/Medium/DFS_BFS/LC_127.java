package Tasks.Medium.DFS_BFS;

import java.util.*;

public class LC_127 {
    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {

        wordList.add(beginWord);
        Map<String, Set<String>> storage = createWordsStorage(wordList);
        Queue<ArrayList<String>> queue = new LinkedList<>();
        ArrayList<String> list = new ArrayList<>();
        list.add(beginWord);
        queue.add(list);

        Set<String> visited = new HashSet<>();
        int steps = 0;

        while (!queue.isEmpty()) {
            steps++;
            ArrayList<String> currLevel = queue.poll();
            ArrayList<String> nextLevel = new ArrayList<>();
            for (String currKey: currLevel) {
                if (currKey == endWord) {
                    return steps;
                }

                if (!visited.contains(currKey)) {
                    visited.add(currKey);
                    for (String vertex: storage.get(currKey)) {
                        nextLevel.add(vertex);
                    }
                }
            }
            if (nextLevel.size() != 0) {
                queue.add(nextLevel);
            }
        }

        return steps;
    }

    private static Map<String, Set<String>> createWordsStorage(List<String> wordList) {
        Map<String, Set<String>> storage = new HashMap<>();
        for (String word: wordList) {
            Set<String> wordsSet = new HashSet<>();
            for (String commonWord: wordList) {
                if (word != commonWord && hasOneUncommonCharacter(word, commonWord) == true) {
                    wordsSet.add(commonWord);
                }
            }
            if (wordsSet.size() != 0) {
                storage.put(word, wordsSet);
            }

        }

        return storage;
    }

    private static boolean hasOneUncommonCharacter(String s1, String s2) {
        int counter = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                 if (counter == 1) {
                     return false;
                 } else {
                     counter++;
                 }
            }
        }

        return true;
    }
}
