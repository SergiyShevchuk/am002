package Tasks.Medium.DFS_BFS;

// 79. Word Search

import java.util.Stack;

public class LC_79 {
    public static boolean exist(char[][] board, String word) {

        if (word == null || word.length() == 0) { return true; }
        if (board == null || board.length == 0 || board[0].length == 0) { return false; }

        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == word.charAt(0)) {
                    Stack<Character> stack = new Stack<>();
                    checkIsPathExist(board,
                            word,
                            stack,
                            0,
                            i,
                            j,
                            directions);

                    if (stack.size() != word.length()) {
                        continue;
                    } else {
                       StringBuilder builder = new StringBuilder();
                       while (!stack.isEmpty()) {
                           builder.append(stack.pop());
                       }
                        if (word.equals(builder.reverse().toString())) {
                            return true;
                        } else {
                            continue;
                        }
                    }
                }
            }
        }

        return false;
    }

    private static void checkIsPathExist(char[][] board,
                                      String word,
                                      Stack<Character> stack,
                                      int letterIndex,
                                      int i,
                                      int j,
                                      int[][] directions) {

        if (word.length() == stack.size()) { return; }

        if ( i < 0 ||
                i >= board.length ||
                j < 0 ||
                j >= board[i].length ||
                board[i][j] != word.charAt(letterIndex)) {

            return; }

        stack.add(board[i][j]);

        for (int[] direction: directions) {
            int x = i + direction[0];
            int y = j + direction[1];
            checkIsPathExist(board, word, stack, letterIndex++, x, y, directions);
        }

        stack.pop();
    }
}
