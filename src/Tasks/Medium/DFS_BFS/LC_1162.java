package Tasks.Medium.DFS_BFS;

/* 1162. As Far from Land as Possible */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_1162 {
    public static int maxDistance(int[][] grid) {
        int distance = 0;

        if (grid == null || grid.length == 0 || grid[0].length == 0) { return distance; }

        ArrayList<int[]> stastPoints = new ArrayList<>();
        boolean[][] visited = new boolean[grid.length][grid[0].length];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    visited[i][j] = true;
                    stastPoints.add(new int[] {i, j});
                }
            }
        }

        if (stastPoints.size() == 0 || stastPoints.size() == grid.length * grid[0].length) { return -1; }
        Queue<ArrayList<int[]>> queue = new LinkedList<>();
        queue.add(stastPoints);
        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        while (!queue.isEmpty()) {
         ArrayList<int[]> currentLevel = queue.poll();
         ArrayList<int[]> nextLevel = new ArrayList<>();
            for (int[] currentPoint: currentLevel) {
                for (int[] direction: directions) {
                    int i = currentPoint[0] + direction[0];
                    int j = currentPoint[1] + direction[1];
                    if (isNextMoveAvailable(grid, visited, i, j, distance + 1)) {
                        nextLevel.add(new int[] {i, j});
                    }
                }
            }

            if (nextLevel.size() != 0) {
                queue.add(nextLevel);
                distance++;
            }
        }

        return distance;
    }

    private static boolean isNextMoveAvailable(int[][] grid, boolean[][] visited, int i, int j, int distance) {
        if (i < 0 ||
                i >= grid.length ||
                j < 0 ||
                j >= grid[i].length ||
                visited[i][j] == true ||
                grid[i][j] >= distance) { return false; }
        visited[i][j] = true;
        grid[i][j] = distance;

        return true;
    }
}

