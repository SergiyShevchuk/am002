package Tasks.Medium.DFS_BFS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_286 {
    public static void wallsAndGates(int[][] rooms) {
        if (rooms == null || rooms.length == 0 || rooms[0].length == 0) { return; }

        Queue<ArrayList<int[]>> queue = new LinkedList<>();
        ArrayList<int[]> gates = new ArrayList<>();

        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[i].length; j++) {
                if (rooms[i][j] == 0) {
                    gates.add(new int[] {i, j});
                }
            }
        }

        if (gates.size() == 0) { return; }
        queue.add(gates);

        int[][] dimensions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        boolean[][] visited = new boolean[rooms.length][rooms[0].length];
        int stepsCounter = 0;

        /*
        INF   -1    0    1
         2    2     1   -1
         1    -1    2   -1
         0    -1    3  INF
         */

        while (!queue.isEmpty()) {
            ArrayList<int[]> currentLevel = queue.poll();
            ArrayList<int[]> nextLevel = new ArrayList<>();
            for (int[] currentMove: currentLevel) {
                visited[currentMove[0]][currentMove[1]] = true;
                int i = currentMove[0];
                int j = currentMove[1];
                for (int[] dimension: dimensions) {
                    int x = i + dimension[0];
                    int y = j + dimension[1];
                    if (isMovePossible(rooms, visited, stepsCounter, x, y)) {
                        nextLevel.add(new int[] {x, y});
                    }
                }
            }

            if (nextLevel.size() != 0) {
                stepsCounter++;
                queue.add(nextLevel);
            }
        }
    }

    private static boolean isMovePossible(int[][] rooms,
                           boolean[][] visited,
                           int stepsCounter,
                           int i,
                           int j) {
        if (i < 0 ||
                i >= rooms.length ||
                j < 0 ||
                j >= rooms[i].length ||
                rooms[i][j] == -1 ||
                visited[i][j] == true ||
                rooms[i][j] == stepsCounter) { return false; }
        rooms[i][j] = stepsCounter + 1;
        return true;
    }
}
