package Tasks.Medium.DFS_BFS;

/* 695. Max Area of Island */

public class LC_695 {
    public static int maxAreaOfIsland(int[][] grid) {
        int maxArea = 0;

        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return maxArea;
        }

        int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    int area = islandArea(grid, i, j, directions);
                    maxArea = Math.max(maxArea, area);
                }
            }
        }

        return maxArea;
    }

    private static int islandArea(int[][] grid,
                                   int i,
                                   int j,
                                   int[][] directions) {

        if (i < 0 ||
                i >= grid.length ||
                j < 0 ||
                j >= grid[i].length ||
                grid[i][j] == 0) {
            return 0;
        }

        int area = 1;
        grid[i][j] = 0;

        for (int[] direction: directions) {
            area += islandArea(grid, i + direction[0], j + direction[1], directions);
        }

        return area;
    }
}
