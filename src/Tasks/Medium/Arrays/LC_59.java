package Tasks.Medium.Arrays;

public class LC_59 {
    public static int[][] generateMatrix(int n) {
        if (n == 1) {
            return new int[][] {{1}};
        }

        int[][] matrix = new int[n][n];

        int top = 0;
        int left = 0;
        int right = n - 1;
        int bottom = n - 1;
        int counter = 0;

        while (top <= bottom && left <= right) {

            for (int coll = left; coll <= right; coll++) {
                counter++;
                matrix[top][coll] = counter;
            }
            top++;

            for (int row = top; row <= bottom; row++) {
                counter++;
                matrix[row][right] = counter;
            }
            right--;

            if (top <= bottom) {
                for (int coll = right; coll >= left; coll--) {
                    counter++;
                    matrix[bottom][coll] = counter;
                }
            }
            bottom--;

            if (left <= right) {
                for (int row = bottom; row >= top; row--) {
                    counter++;
                    matrix[row][left] = counter;
                }
            }
            left++;
        }

        return matrix;
    }
}
