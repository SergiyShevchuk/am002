package Tasks.Medium.Arrays;

import java.util.HashMap;

public class LC_957 {
    public static int[] prisonAfterNDays(int[] cells, int N) {

        HashMap<Integer, Integer> cellsStates = new HashMap<>();
        HashMap<Integer, int[]> states = new HashMap<>();

        int pointer = 0;
        for (int i: cells) {
            cellsStates.put(pointer++, i);
        }

        if (N <= 256) {
            calculateState(cells, cellsStates, states, N);
        } else {
            calculateState(cells, cellsStates, states, 256);
            int state = N - 1 % 256;
            int[] currState = states.get(state);
            return currState;
        }

        return cells;
    }

    // 1,0,0,1,0,0,1,0

    private static void calculateState(int[] cells,
                                       HashMap<Integer, Integer> cellsStates,
                                       HashMap<Integer,
                                               int[]> states, int N) {
        for (int i = 0; i < N; i++) {
            states.put(i, cells);
            for (int j = 0; j < cells.length; j++) {
                if (j + 2 >= cells.length) {
                    cells[0] = 0;
                    cells[cells.length - 1] = 0;
                    int pointerX = 0;
                    for (int x: cells) {
                        cellsStates.put(pointerX++, x);
                    }
                    break;
                } else {
                    if (cellsStates.get(j) == cellsStates.get(j + 2)) {
                        cells[j + 1] = 1;
                    } else {
                        cells[j + 1] = 0;
                    }
                }
            }
        }
    }
}
