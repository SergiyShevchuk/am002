package Tasks.Medium.Arrays;

// 973. K Closest Points to Origin

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class LC_973 {
    public static int[][] kClosest(int[][] points, int K) {

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        HashMap<Integer, ArrayList<int[]>> storage = new HashMap<>();

        for (int[] point: points) {
            int distance = distanceToOrigin(point[0], point[1]);
            priorityQueue.add(distance);
            ArrayList<int[]> distances = storage.getOrDefault(distance, new ArrayList<>());
            distances.add(point);
            storage.put(distance, distances);
        }

        int[][] retValue = new int[K][2];

        for (int i = 0; i < K; i++) {
            int distance = priorityQueue.poll();
            ArrayList<int[]> distances = storage.get(distance);
            retValue[i] = distances.get(0);
            distances.remove(0);
            if (distances.size() == 0) {
                storage.remove(distance);
            } else {
                storage.put(distance, distances);
            }
        }

        return retValue;
    }

    private static int distanceToOrigin(int x, int y) {
        return (x * x) + (y * y);
    }
}
