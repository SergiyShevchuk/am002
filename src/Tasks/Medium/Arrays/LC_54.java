package Tasks.Medium.Arrays;

import java.util.*;

public class LC_54 {
    public static List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> list = new ArrayList<>();

        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return list;
        }

        if (matrix.length == 1) {
            for (int value: matrix[0]) {
                list.add(value);
            }

            return list;
        }

        int top = 0;
        int bottom = matrix.length - 1; // 3

        int right = matrix[0].length - 1; // 3
        int left = 0;

        while (top <= bottom && left <= right) {

            for (int i = left; i <= right; i++) {
                list.add(matrix[top][i]);
            }

            top++;

            for (int i = top; i <= bottom; i++) {
                list.add(matrix[i][right]);
            }

            // 1.3

            right--;

            if (top <= bottom) {
                for (int i = right; i >= left; i--) {
                    list.add(matrix[bottom][i]);
                }
            }
            bottom--;


            if (left <= right) {
                for (int i = bottom; i >= top; i--) {
                    list.add(matrix[i][left]);
                }
                left++;
            }
        }

        return list;
    }
}
