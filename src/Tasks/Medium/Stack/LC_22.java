package Tasks.Medium.Stack;

// 22. Generate Parentheses

import java.util.*;

public class LC_22 {
    public static List<String> generateParenthesis(int n) {
        List<String> retValue = new ArrayList<>();
        if (n == 0) { return retValue; }
        if (n == 1) {
            retValue.add("()");
            return retValue; }

        String S = "";

        StringBuilder builder = new StringBuilder();
        Set<Character> set = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));
        for (Character current: S.toCharArray()) {
            if (!set.contains(current)) {
                builder.append(current);
            }
        }





        builder.toString();

        backtracking(retValue, "", 0, 0, n);

        return retValue;
    }

    private static void backtracking(List<String> list, String current, int left, int right, int max) {
        if (current.length() == max * 2) {
            list.add(current);
            return;
        }

        if (left < max) {
            backtracking(list, current + "(", left + 1, right, max);
        }
        if (right < left) {
            backtracking(list, current + ")", left, right + 1, max);
        }
    }
}
