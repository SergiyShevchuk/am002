package Tasks.Medium;

/* 200. Number of Islands */

public class LC_200 {
    public static int numIslands(char[][] grid) {

        /*
        11110
        11010
        11000
        00000
         */

        int counter = 0;
        if (grid == null) { return counter; }
        if (grid.length == 0 || grid[0].length == 0) { return counter; }

        int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    counter++;
                    DFSSearch(grid, i, j, directions);
                }
            }
        }

        return counter;
    }

    private static void DFSSearch(char[][] grid, int i, int j, int[][] directions) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[i].length || grid[i][j] != '1') { return; }

        grid[i][j] = '0';

        for (int[] direction: directions) {
            DFSSearch(grid, i + direction[0], j + direction[1], directions);
        }
    }
}
