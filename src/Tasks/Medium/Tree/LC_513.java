package Tasks.Medium.Tree;
import Common.DataStructure.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LC_513 {
    public int findBottomLeftValue(TreeNode root) {

        Queue<List<TreeNode>> queue = new LinkedList<>();
        List<TreeNode> zeroLevel = new ArrayList<>();
        zeroLevel.add(root);
        List<TreeNode> prevLevel = new ArrayList<>();
        prevLevel = zeroLevel;

        while (!queue.isEmpty()) {
            List<TreeNode> currLevel = queue.poll();
            List<TreeNode> nextLevel = new ArrayList<>();
            for (TreeNode currNode: currLevel) {
                if (currNode.left != null) { nextLevel.add(currNode.left); }
                if (currNode.right != null) { nextLevel.add(currNode.right); }
            }

            if (nextLevel.size() != 0) {
                prevLevel = currLevel;
                queue.add(nextLevel);
            } else {
                return prevLevel.get(0).left.val;
            }
        }

        return 0;
    }
}
