package Tasks.Medium.Grafs;
import java.util.*;

public class LC_841 {
    public static boolean canVisitAllRooms(List<List<Integer>> rooms) {

        Set<Integer> visited = new HashSet<>();
        Queue<Integer> queue = new LinkedList<>();
        queue.add(0);
        visited.add(0);

        while (!queue.isEmpty()) {
            Integer room = queue.poll();
            List<Integer> keys = rooms.get(room);
            for (Integer key: keys) {
                if (!visited.contains(key)) {
                    queue.add(key);
                    visited.add(key);
                }
            }
        }

        return rooms.size() == visited.size();
    }
}
