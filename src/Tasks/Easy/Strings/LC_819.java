package Tasks.Easy.Strings;

// 819. Most Common Word


/*
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
banned = ["hit"]
Output: "ball"
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class LC_819 {
    public static String mostCommonWord(String paragraph, String[] banned) {
        Set<String> bannedSet = new HashSet<>(Arrays.asList(banned));
        HashMap<String, Integer> storage = new HashMap<>();
        int maxPopular = 0;

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < paragraph.length(); i++) {
            Character current = paragraph.charAt(i);
            if (Character.isLetter(current)) {
                builder.append(Character.toLowerCase(current));
            } else if (builder.length() != 0) {
                String word = builder.toString();
                maxPopular = storeNewWord(builder.toString(), bannedSet, storage, maxPopular);
                builder = new StringBuilder();
            }
        }

        if (builder.length() != 0) {
            maxPopular = storeNewWord(builder.toString(), bannedSet, storage, maxPopular);
        }

        for (String word: storage.keySet()) {
            if (storage.get(word) == maxPopular) {
                return word;
            }
        }

        return "";
    }

    private static int storeNewWord(String word,
                                     Set<String> bannedSet,
                                     HashMap<String, Integer> storage,
                                    int maxPopular) {
        if (!bannedSet.contains(word)) {
            storage.put(word, storage.getOrDefault(word, 0) + 1);
            return Math.max(maxPopular, storage.get(word));
        }

        return Math.max(0, maxPopular);
    }
}
