package Tasks.Easy.Strings;

public class LC_482 {
    public static String licenseKeyFormatting(String S, int K) {
        StringBuilder builder = new StringBuilder();

        int shifPoint = K;
       for (int i = S.length() - 1; i >= 0; i--) {
           if (S.charAt(i) == '-') { continue; }
           if (shifPoint == 0) {
               builder.append('-');
               shifPoint = K;
           }
           builder.append(Character.toUpperCase(S.charAt(i)));
           shifPoint--;
       }

       return builder.reverse().toString();
    }
}
