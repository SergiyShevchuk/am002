package Tasks.Easy.Strings;

// 1047. Remove All Adjacent Duplicates In String

import java.util.Stack;

public class LC_1047 {
    public static String removeDuplicates(String S) {
        if (S.length() == 1) { return S; }

        // abbaca

        Character lastChar = S.charAt(0);
        Stack<Character> stack = new Stack<>();
        stack.add(lastChar);

        for (int i = 1; i < S.length(); i++) {
            Character current = S.charAt(i);
            if (current != lastChar) {
                stack.add(current);
                lastChar = current;
            } else {
                stack.pop();
                if (stack.isEmpty()) {
                    lastChar = '1';
                } else {
                    lastChar = stack.peek();
                }
            }
        }

        StringBuilder builder = new StringBuilder();
        while (!stack.isEmpty()) {
            builder.append(stack.pop());
        }

        return builder.reverse().toString();
    }
}
