package Tasks.Easy.Strings;

// 682. Baseball Game

import java.util.Stack;

public class LC_682 {
    public static int calPoints(String[] ops) {
        int summ = 0;

        if (ops == null || ops.length == 0) { return summ; }

        Stack<Integer> stack = new Stack<>();

        for (String currentMove: ops) {
            if (currentMove == "+") {
                if (!stack.isEmpty()) {
                    int last = stack.pop();
                    if (!stack.isEmpty()) {
                        int first = stack.pop();
                        summ += first + last;
                        stack.add(first);
                        stack.add(last);
                        stack.add(first + last);
                    } else {
                        summ += last + last;
                        stack.add(last);
                        stack.add(last + last);
                    }
                }
            } else if (currentMove == "D") {
                if (!stack.isEmpty()) {
                    int last = stack.pop();
                    summ += last * 2;
                    stack.add(last);
                    stack.add(last * 2);
                }
            } else if (currentMove.equals("C")) {
                int removedPoints = stack.pop();
                summ -= removedPoints;
            } else {
                int currentDigit = convertString(currentMove);
                stack.add(currentDigit);
                summ += currentDigit;
            }
        }

        return summ;
    }

    private static int convertString(String string) {
        int value = 0;
        int shiftPoint = string.charAt(0) == '-' ? 1 : 0;
        for (int i = shiftPoint; i < string.length(); i++) {
            Character current = string.charAt(i);
            Integer currDigit = Integer.valueOf(String.valueOf(current));
            value = (value * 10) + currDigit;
        }

        value = shiftPoint == 0 ? value : value * -1;

        return value;
    }
}
