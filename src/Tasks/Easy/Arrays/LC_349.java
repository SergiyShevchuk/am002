package Tasks.Easy.Arrays;

import java.util.*;

public class LC_349 {
    public static int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> first = new HashSet<>();
        Set<Integer> second = new HashSet<>();

        for (int value: nums1) {
            first.add(value);
        }

        for (int value: nums2) {
            second.add(value);
        }

        first.retainAll(second);

        Integer[] sasas = new Integer[first.size()];
        first.toArray(sasas);
        int[] retValue = new int[first.size()];
        for (int i = 0; i < sasas.length; i++) {
            retValue[i] = sasas[i];
        }

        return retValue;
    }
}
