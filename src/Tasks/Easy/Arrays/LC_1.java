package Tasks.Easy.Arrays;

import java.util.HashMap;
import java.util.Map;

// [3, 2, 4]

public class LC_1 {
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> indexes = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            indexes.put(nums[i], i);
        }

        int[] retValue = new int[2];

        for (int i = 0; i < nums.length; i++) {
            if (indexes.containsKey(target - nums[i]) && indexes.get(target - nums[i]) != i) {
                retValue = new int[] {i, indexes.get(target - nums[i])};
                return retValue;
            }
        }

        return retValue;
    }
}
