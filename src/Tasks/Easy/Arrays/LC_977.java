package Tasks.Easy.Arrays;

import java.util.*;

public class LC_977 {
    public int[] sortedSquares(int[] A) {
        for (int i = 0; i < A.length; i++) {
            int value = A[i] * A[i];
            A[i] = value;
        }

        Arrays.sort(A);

        return A;
    }

    public int[] sortedSquaresOptimalSolution(int[] A) {
        if (A.length == 1) {
            return A;
        }

        int[] retValue = new int[A.length];
        int left = 0;
        int right = A.length - 1;

        for (int i = 0; i < A.length; i++) {
            if (Math.abs(A[left]) > Math.abs(A[right])) {
                retValue[A.length - 1 - i] = A[left] * A[left];
                left++;
            } else {
                retValue[A.length - 1 - i] = A[right] * A[right];
                right--;
            }
        }


        return retValue;
    }
}
