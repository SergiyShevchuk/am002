package Tasks.Easy.Arrays;

public class LC_1099 {
    public static int twoSumLessThanK(int[] A, int K) {

        int maxSumm = -1;

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                if (i != j) {
                    if (A[i] + A[j] < K) {
                        maxSumm = Math.max(maxSumm, A[i] + A[j]);
                    }
                }
            }
        }


        return maxSumm;
    }
}
