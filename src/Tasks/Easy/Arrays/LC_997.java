package Tasks.Easy.Arrays;

/*
1 <= N <= 1000
trust.length <= 10000
trust[i] are all different
trust[i][0] != trust[i][1]
1 <= trust[i][0], trust[i][1] <= N
 */


// N = 3, trust = [[1,3],[2,3]]

public class LC_997 {
    public int findJudge(int N, int[][] trust) {

        int[] votes = new int[N + 1];
        int[] voted = new int[N + 1];

        for (int i = 0; i < trust.length; i++) {
            int[] curretVoice = trust[i];
            votes[curretVoice[0]]++;
            voted[curretVoice[1]]++;
        }

        for (int i = 1; i <= N; i++) {
            if (votes[i] == 0 && voted[i] == N) {
                return i;
            }
        }

        return -1;
    }
}
