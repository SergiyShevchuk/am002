package Tasks.Easy.Stack;

import java.util.Stack;

public class LC_20 {

    /*
    Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
    determine if the input string is valid.
     */

    public boolean isValid(String s) {
        if (s == null || s.length() == 0) { return true; }
        if (s.length() == 1) { return false; }

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            Character current = s.charAt(i);
            if (current == '(' || current == '{' || current == '[') {
                stack.add(current);
            } else {
                if (stack.isEmpty()) { return false; }
                if (current == ')' && stack.pop() != '(') { return false; }
                if (current == '}' && stack.pop() != '{') { return false; }
                if (current == ']' && stack.pop() != '[') { return false; }
            }
        }

        return stack.isEmpty();
    }
}
