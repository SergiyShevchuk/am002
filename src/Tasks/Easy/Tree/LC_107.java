package Tasks.Easy.Tree;

import Common.DataStructure.TreeNode;

import java.util.*;

public class LC_107 {
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> storage = new LinkedList<>();
        if (root == null) { return storage; }
        if (root.left == null && root.right == null) {
            List<Integer> list = new LinkedList<>();
            list.add(root.val);
            storage.add(list);
            return storage;
        }

        Stack<List<Integer>> levelsQueue = new Stack<>();
        Queue<ArrayList<TreeNode>> queue = new LinkedList<>();
        ArrayList<TreeNode> fisrtLevel = new ArrayList<>();
        fisrtLevel.add(root);
        queue.add(fisrtLevel);

        while (!queue.isEmpty()) {
            ArrayList<TreeNode> currentLevel = queue.poll();
            processNodesLevel(currentLevel, levelsQueue);
            ArrayList<TreeNode> nextLevel = new ArrayList<>();
            for (TreeNode node: currentLevel) {
                if (node.left != null) {
                    nextLevel.add(node.left);
                }
                if (node.right != null) {
                    nextLevel.add(node.right);
                }
            }

            if (nextLevel.size() != 0) {
                queue.add(nextLevel);
            }
        }

        while (!levelsQueue.isEmpty()) {
            storage.add(levelsQueue.pop());
        }

        return storage;
    }

    private void processNodesLevel(ArrayList<TreeNode> level, Stack<List<Integer>> queue) {
        List<Integer> stackLevel = new LinkedList<>();
        for (TreeNode node: level) {
            stackLevel.add(node.val);
        }
        queue.add(stackLevel);
    }
}
