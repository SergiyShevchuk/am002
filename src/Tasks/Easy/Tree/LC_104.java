package Tasks.Easy.Tree;

// 104. Maximum Depth of Binary Tree

import Common.DataStructure.TreeNode;

public class LC_104 {
    public int maxDepth(TreeNode root) {
        if ( root == null) { return 0; }
        return Math.max(maxDepth(root.left) + 1, maxDepth(root.right) + 1);
    }
}
