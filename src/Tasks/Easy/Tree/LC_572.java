package Tasks.Easy.Tree;

// 572. Subtree of Another Tree

import Common.DataStructure.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class LC_572 {

    public boolean isSubtree(TreeNode s, TreeNode t) {
        return traverse(s, t);
    }

    private boolean traverse(TreeNode s, TreeNode t) {
        return s != null && (checkIsSubtree(s, t) || traverse(s.left, t) || traverse(s.right, t));
    }

    private boolean checkIsSubtree(TreeNode s, TreeNode t) {
        if (s == null && t == null) { return true; }
        if (s == null || t == null) { return false; }
        return s.val == t.val && checkIsSubtree(s.left, t.left) && checkIsSubtree(s.right, t.left);
    }
}
