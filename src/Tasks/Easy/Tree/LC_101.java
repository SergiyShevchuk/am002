package Tasks.Easy.Tree;

import Common.DataStructure.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_101 {
    public boolean isSymmetric(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) { return true; }

        return isLevelSymmetric(root.left, root.right);
    }

    private boolean isLevelSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) { return true; }
        if (left == null || right == null) { return false; }

        return left.val == right.val &&
                isLevelSymmetric(left.left, right.right) &&
                isLevelSymmetric(left.right, right.left);
    }
}
