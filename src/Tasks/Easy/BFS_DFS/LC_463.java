package Tasks.Easy.BFS_DFS;

/* 463. Island Perimeter */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_463 {
    public static int islandPerimeter(int[][] grid) {
        int perimeter = 0;
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return perimeter;
        }

        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    for (int[] direction : directions) {
                        int x = direction[0];
                        int y = direction[1];
                        if (x < 0 || x >= grid.length || y < 0 || y >= grid[x].length || grid[x][y] == 0)
                            perimeter++;
                    }
                }
            }
        }
        return perimeter;
    }
}