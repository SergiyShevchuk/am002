package Tasks.Easy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_994 {
    public int orangesRotting(int[][] grid) {
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return -1;
        }

        ArrayList<ArrayList<Integer>> rottens = new ArrayList<>();
        int rottenCount = 0;
        int emptyCount = 0;
        int freshCount = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 2) {
                    rottenCount++;
                    ArrayList<Integer> rotten = new ArrayList<>();
                    rotten.add(i);
                    rotten.add(j);
                    rottens.add(rotten);
                } else if (grid[i][j] == 1) {
                    freshCount++;
                } else if (grid[i][j] == 0) {
                    emptyCount++;
                }
            }
        }

        int cellsAmount = grid.length * grid[0].length;
        if (rottenCount == cellsAmount ||
                emptyCount == cellsAmount ||
                rottenCount + emptyCount == cellsAmount) { return 0; }
        if (freshCount == cellsAmount) { return -1; }

        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        Queue<ArrayList<ArrayList<Integer>>> queue = new LinkedList<>();
        queue.add(rottens);

        int steps = 0;

        while (!queue.isEmpty()) {
            ArrayList<ArrayList<Integer>> rottenPoints = queue.poll();
            ArrayList<ArrayList<Integer>> nextWave = new ArrayList<>();
            for (ArrayList<Integer> rottenOrange: rottenPoints) {
                for (int[] direction: directions) {
                    if (isRottenPossible(grid,
                            rottenOrange.get(0) + direction[0],
                            rottenOrange.get(1) + direction[1])) {
                        ArrayList<Integer> nextRotten = new ArrayList<>();
                        nextRotten.add(rottenOrange.get(0) + direction[0]);
                        nextRotten.add(rottenOrange.get(1) + direction[1]);
                        nextWave.add(nextRotten);
                    };
                }
            }

            if (nextWave.size() != 0) {
                steps++;
                queue.add(nextWave);
            }
        }

        if (freshCount >= 1) {
            return -1;
        }

        if (steps >= 1) {
            return steps;
        }

        return -1;
    }


    private boolean isRottenPossible(int[][] grid, int i, int j) {
        if (i < 0 ||
                i >= grid.length ||
                j < 0 ||
                j >= grid[i].length ||
                grid[i][j] == 2 ||
                grid[i][j] == 0) {
            return false;
        }

        grid[i][j] = 2;

        return true;
    }
}
